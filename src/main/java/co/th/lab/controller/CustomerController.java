package co.th.lab.controller;

import java.util.List;

import javax.persistence.EntityManager;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import co.th.lab.entities.Customer;

@Controller
public class CustomerController {
	
	@Autowired JpaTransactionManager transactionManager;
	
	@RequestMapping(value="/customerFind")
	public @ResponseBody List<Customer> getAllCustomer(){
		
		EntityManager entityManager = transactionManager.getEntityManagerFactory().createEntityManager();
		try {
			entityManager.getTransaction().begin();
			
			List<Customer> customer = entityManager.createNamedQuery("Customer.findAll", Customer.class).getResultList();
			
			entityManager.getTransaction().commit();
			return customer;
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			entityManager.getTransaction().rollback();
			return null;
		}finally {
			entityManager.close();
		}
		
	}
}
