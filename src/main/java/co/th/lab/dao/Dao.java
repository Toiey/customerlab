package co.th.lab.dao;


public interface Dao<K, E> {
	public void save(E entities);
	public void remove(E entities);
	public E findById(K Id);
	void update(E entities, K Id);
}
