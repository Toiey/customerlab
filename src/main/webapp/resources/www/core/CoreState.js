angular.module("StateModule", [
	'ui.router',
	'oc.lazyLoad'
]).config(function(
    $stateProvider,
    $urlRouterProvider,
    $ocLazyLoadProvider
){
	
	$urlRouterProvider.otherwise("/home");

	$stateProvider
        .state('home', {
		url : "/home",
		templateUrl : "page/home/template/template-main.html",
		controller: 'HomeController',
            controllerAs: 'THIS',
            resolve: {
                loadMyCtrl: ['$ocLazyLoad', function($ocLazyLoad) {
                    // you can lazy load files for an existing module
                    return $ocLazyLoad.load({
                        files: [
                            "page/home/controller/controller-main.js",
                        ]
                    });
                }]
            }
	   })
       .state('customer', {
            url: "/customer",
            templateUrl: "page/customer/template/template-main.html",
            controller: 'CustomerController',
            controllerAs: 'THIS',
            resolve: {
                loadMyCtrl: ['$ocLazyLoad', function($ocLazyLoad) {
                    // you can lazy load files for an existing module
                    return $ocLazyLoad.load({
                        files: [
                            "page/customer/controller/controller-main.js",
                            "page/customer/service/service-main.js"
                        ]
                    });
                }]
            }
        })
        
        var result = [{
        	"stateName": "user",
        	"stateUrl": "/user",
        	"templateUrl": "page/user/template/template-main.html",
        	"controllerName": "UserController",
        	"files":[
        	       "page/user/controller/controller-main.js",
        	       "page/user/service/service-main.js"
        	]
        },{
        	"stateName": "userinsert",
        	"stateUrl": "/userinsert",
        	"templateUrl": "page/user/template/template-insert.html",
        	"controllerName": "insertController",
        	"files":[
        	       "page/user/controller/controller-insert.js",
        	       "page/user/service/service-main.js"
        	]
        }];
	
	
		result.forEach(function(element){
			var temp = {
					url: element.stateUrl,
					templateUrl: element.templateUrl,
					controllerAs: 'THIS'
			};
			
			if(element.templateUrl){
				temp.templateUrl = element.templateUrl;
			}
			if(element.files){
				temp.resolve = {
					loadMyCtrl: ['$ocLazyLoad', function($ocLazyLoad){
						return $ocLazyLoad.load({
							files: element.files
						});
					}]
				}
			}
			if(element.controllerName){
				temp.controller = element.controllerName;
			}
			if(element.params){
				temp.params = element.params;
			}
			$stateProvider.state(element.stateName, temp);
		});
	
}).run(function($rootScope,$uibModalStack){
	
	$rootScope.$on('$stateChangeStart', function(event, toState, toParams, fromState, fromParams) {
        console.log(fromState.name + "-> " + toState.name);
        $rootScope.fromState = fromState.name;
        $uibModalStack.dismissAll();
    });

    $rootScope.$on('$stateChangeSuccess', function(event, toState, toParams, fromState, fromParams) {
    	console.log(toState.name);
        $rootScope.currentState = toState.name;
        $rootScope.isCollapse = true;
    });
	
})